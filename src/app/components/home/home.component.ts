import { Component, Renderer2, RendererFactory2, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  canvas: any;
  ctx: any;
  message = '';
  imagePath: any;
  url: any;
  renderer: any;
  imgForm: any;
  xCoordinate = false;
  yCoordinate = false;
  width = false;
  height = false;
  form = false;

  //array of object which consists coordinate values
  coordinates = [{
    "x-coordinate": 120,
    "y-coordinate": 0
  }, {
    "x-coordinate": 50,
    "y-coordinate": 110
  }]

  constructor(private rendererFactory: RendererFactory2) {
    this.renderer = this.rendererFactory.createRenderer(null, null);
  }

  ngOnInit(): void {
    this.canvas = <HTMLCanvasElement>document.getElementById("myCanvas");
    this.ctx = this.canvas.getContext("2d");
    this.imgForm = new FormGroup({
      x_coordinate: new FormControl('', Validators.required),
      y_coordinate: new FormControl('', Validators.required),
      width: new FormControl('', Validators.required),
      height: new FormControl('', Validators.required)
    })
  }

  //method for form submission
  submitForm() {
    let forms = [this.imgForm];
    let validation = this.imgForm.invalid;
    let newImg = this.renderer.createElement('img');
    newImg.src = this.url;
    
    setTimeout(() => {
      if (this.imgForm.value["width"] > newImg.width) {
        this.width = true;
        return
      } else if (this.imgForm.value["height"] > newImg.height) {
        this.height = true;
        return;
      } else if ((this.imgForm.value["x_coordinate"]-newImg.width) > 0) {
        this.xCoordinate = true;
        return;
      } else if ((this.imgForm.value["y_coordinate"]-newImg.height) > 0) {
        this.yCoordinate = true;
        return;
      }else{
        this.width = false;
        this.height = false;
        this.xCoordinate = false;
        this.yCoordinate = false;
      }
      if (validation) {
        forms.map((formName: any) => {
          Object.keys(formName.controls).forEach(key => {
            let form: any = formName.get(key);
            form.touched = true;
          })
        })
        return
      }
      newImg.onload = this.drawImg(newImg);
    }, 500)
  }

  //Method for create shapes on uploaded images
  drawImg(imgContext: any) {
    setTimeout(() => {
      this.canvas.width = imgContext.width;
      this.canvas.height = imgContext.height;
      this.ctx.drawImage(imgContext, 0, 0, this.canvas.width, this.canvas.height);
      for (var i = 0; i < this.coordinates.length; i++) {
        var context = this.canvas.getContext('2d');
        var borderColor = this.getRandomColor();
        context.beginPath();
        //data comes from form
        context.rect(this.imgForm.value["x_coordinate"], this.imgForm.value["y_coordinate"], this.imgForm.value["width"], this.imgForm.value["height"]);
        context.fillStyle = 'transparent';
        context.fill();
        context.lineWidth = 3;
        context.strokeStyle = borderColor;
        context.stroke();
      }
    }, 1000);

  }

  //method for getting Random Color
  getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  //Method to validate whether uploaded files are images or not 
  onFileChanged(event: any) {
    const files = event.target.files;
    if (files.length === 0)
      return;

    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }

    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.url = reader.result;
      this.message = '';
      this.form = true;
    }
  }

}
