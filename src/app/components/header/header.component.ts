import { Component, OnInit } from '@angular/core';

declare var $: any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  mobileHeaderFlag = 0;
  visible1 = false;
  constructor() { }

  ngOnInit(): void {
  }

  openNav(){
    if(this.mobileHeaderFlag === 0){
        $('#navbarMobileDefault .navbar-nav').css('width','60%');
        $('#sidenav-overlay').css('display','block');
        $('.close-btn-header').css({'display':'block','transition':'0.6s ease'});
        this.mobileHeaderFlag = 1;
    }else if(this.mobileHeaderFlag === 1){
        $('#navbarMobileDefault .navbar-nav').css('width','0');
        $('#sidenav-overlay').css('display','none');
        $('.close-btn-header').css({'display':'none','transition':'0.6s ease'});
        this.mobileHeaderFlag = 0;
    }
  }

}
